class CreateMentorships < ActiveRecord::Migration[5.1]
  def change
    create_table :mentorships do |t|
      t.references :goal, foreign_key: true
      t.references :article, foreign_key: true

      t.timestamps
    end
  end
end
