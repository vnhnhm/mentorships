class Goal < ApplicationRecord
  belongs_to :user
  has_many :mentorships
  has_many :articles, through: :mentorships
end
