class User < ApplicationRecord
  has_many :mentorships
  has_many :goals
  has_many :articles
end
