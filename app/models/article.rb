class Article < ApplicationRecord
  belongs_to :user
  has_many :mentorships
  has_many :goals, through: :mentorships
end
