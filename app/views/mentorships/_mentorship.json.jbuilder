json.extract! mentorship, :id, :goal_id, :article_id, :created_at, :updated_at
json.url mentorship_url(mentorship, format: :json)
