class MentorshipsController < ApplicationController
  before_action :set_mentorship, only: [:show, :edit, :update, :destroy]

  def index
    @mentorships = Mentorship.all
  end

  def show
  end

  def new
    @mentorship = Mentorship.build
    @goals = User.find(1).goals
  end

  def edit
  end

  def create
    @mentorship = Mentorship.new(mentorship_params)

    respond_to do |format|
      if @mentorship.save
        format.html { redirect_to @mentorship, notice: 'Mentorship was successfully created.' }
        format.json { render :show, status: :created, location: @mentorship }
      else
        format.html { render :new }
        format.json { render json: @mentorship.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @mentorship.update(mentorship_params)
        format.html { redirect_to @mentorship, notice: 'Mentorship was successfully updated.' }
        format.json { render :show, status: :ok, location: @mentorship }
      else
        format.html { render :edit }
        format.json { render json: @mentorship.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @mentorship.destroy
    respond_to do |format|
      format.html { redirect_to mentorships_url, notice: 'Mentorship was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_mentorship
      @mentorship = Mentorship.find(params[:id])
    end

    def mentorship_params
      params.require(:mentorship).permit(:goal_id, :article_id)
    end
end
